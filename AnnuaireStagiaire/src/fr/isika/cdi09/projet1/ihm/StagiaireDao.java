package fr.isika.cdi09.projet1.ihm;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import fr.isika.cdi09.projet1.objects.ArbreStagiaire;
import fr.isika.cdi09.projet1.objects.EcritureBinaire;
import fr.isika.cdi09.projet1.objects.ExtractDataFileBin;
import fr.isika.cdi09.projet1.objects.LectureFichierStagiaire;
import fr.isika.cdi09.projet1.objects.NoeudStagiaire;
import fr.isika.cdi09.projet1.objects.Stagiaire;


public class StagiaireDao {
	
	private String fichierBinPath = "STAGIAIRES.bin";
	private ExtractDataFileBin extractDataFileBin;
	private EcritureBinaire ecritureBinaire;
	private ArbreStagiaire arbre;
	private int numero;
	
	public StagiaireDao() {
		// vérifier si le fichier binaire existe
		File fichierBin = new File(fichierBinPath);
		if(fichierBin.exists()) {
			// charger l'arbre à partir du fichier 
			extractDataFileBin = new ExtractDataFileBin();
			arbre = extractDataFileBin.extract(fichierBinPath);
		} else {
			// créer le fichier bineaire à partir de l'arbre
			ecritureBinaire = new EcritureBinaire();
			initArbre();
		}
	}
	
	private void initArbre() {
		List<Stagiaire> stagiaires = getStagiairesFromDon();
		arbre = new ArbreStagiaire();
		for(int i = 0; i <= stagiaires.size()-1; i++) {
			Stagiaire val = stagiaires.get(i);
			NoeudStagiaire noeudStagiaire = new NoeudStagiaire(val);
			arbre.ajouterNoeud(noeudStagiaire);
			ecritureBinaire.ecrireNouveauNoeud(noeudStagiaire, fichierBinPath);
		}
	}
	
	public List<Stagiaire> getStagiairesFromArbre() {
		List<Stagiaire> liste = new ArrayList<>(); 
		arbre.parcousInfixe1(arbre.getRacine(), liste);
		return liste;
	}
	
	public List<Stagiaire> getStagiairesFromDon() {
		return  LectureFichierStagiaire.lectureFichierStagiaire(new File("STAGIAIRES.DON"));
	}
	
	public NoeudStagiaire ajouterStagiaire(Stagiaire stagiaire) {
		stagiaire.setIndice(getStagiairesFromArbre().size());
		NoeudStagiaire newnoeud = new NoeudStagiaire(stagiaire);
		this.arbre.ajouterNoeud(newnoeud);
		ecritureBinaire = new EcritureBinaire();
		ecritureBinaire.ecrireNouveauNoeud(newnoeud, fichierBinPath);
		return newnoeud;
	}
	public NoeudStagiaire MiseAJourStagiaire(Stagiaire stagiaire) {
		NoeudStagiaire uptnoeud = this.arbre.Get_Noeud(stagiaire,arbre.getRacine());  
		// TODO this.arbre.Mise a jour (newnoeud);
		 uptnoeud.setVal(stagiaire);
		ecritureBinaire = new EcritureBinaire();
		ecritureBinaire.MiseAJourNoeud(uptnoeud, fichierBinPath);
		return uptnoeud;
	}
	public NoeudStagiaire SupprimerStagiaire(Stagiaire stagiaire) {
		NoeudStagiaire delnoeud = this.arbre.Get_Noeud(stagiaire,arbre.getRacine());  		
		ecritureBinaire = new EcritureBinaire();
		ecritureBinaire.SuppressionNoeud(delnoeud, fichierBinPath);
		arbre.supprimer(stagiaire, arbre.getRacine());
		return delnoeud;
	}
	/*
	public void rechercherStagiaire(String nom) {
		
		Stagiaire s = new Stagiaire(nom);
		this.arbre.recherche(s, arbre.getRacine(), getStagiairesFromArbre());
		
		//this.arbre.recherche(stagiaire, arbre.getRacine(), getStagiairesFromArbre());
	}
*/
}
