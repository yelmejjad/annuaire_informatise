package fr.isika.cdi09.projet1.ihm;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class SecuriteSuppression extends GridPane {
	
	private Label lblSuppression = new Label("Etes vous sûr de vouloir supprimer ce stagiaire ?");

	private Button btnValider = new Button("Valider");
	private Button btnAnnuler = new Button("Annuler");

	public SecuriteSuppression() {
		
		setPrefSize(500,200);
		add(lblSuppression,1,0);
		add(btnValider, 0,2);
		add(btnAnnuler, 2,2);
		setAlignment(Pos.CENTER);
		setHgap(-150);
		setVgap(20);
		
		btnValider.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				//on instancie le nouveau pane
				FenetrePrincipale firstPane = new FenetrePrincipale();
				// on crée une nouvelle scene avec le nouveau panneau
				Scene firstScene = new Scene(firstPane);
				// on recupere le stage
				Stage stage = (Stage) SecuriteSuppression.this.getScene().getWindow();
				stage.setTitle("Annuaire");
				stage.setScene(firstScene);
			}
		});
		
		
		btnAnnuler.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				//on instancie le nouveau pane
				FenetrePrincipale firstPane = new FenetrePrincipale();
				// on crée une nouvelle scene avec le nouveau panneau
				Scene firstScene = new Scene(firstPane);
				// on recupere le stage
				Stage stage = (Stage) SecuriteSuppression.this.getScene().getWindow();
				stage.setTitle("Annuaire");
				stage.setScene(firstScene);
			}
		});
		
	}
	
	

}
