package fr.isika.cdi09.projet1.ihm;

import fr.isika.cdi09.projet1.objects.Stagiaire;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;


public class InterfaceAjouter extends GridPane {

	private Label labelNom = new Label("Nom : ");
	private TextField txtNom = new TextField();

	private Label labelPrenom = new Label("Prénom : ");
	private TextField txtPrenom = new TextField();

	private Label labelDepartement = new Label("Département : ");
	private TextField txtDepartement = new TextField();

	private Label labelPromotion= new Label("Promotion : ");
	private TextField txtPromotion = new TextField();

	private Label labelAnnee = new Label("Année : ");
	private TextField txtAnne = new TextField();

	private Button btnValider = new Button("Valider");
	private Button btnReset = new Button("Reset");
	private Button btnTest = new Button("Test");

	private FenetrePrincipale fenetrePrincipale;
	private Stage stage;

	public InterfaceAjouter() {

		setPrefSize(450,500);
		setStyle("-fx-background-color: #c0c5ce");
		
		btnValider.setPrefSize(80,25);
		btnValider.setStyle("-fx-background-color: #65737e;-fx-text-fill: #c0c5ce;-fx-font-weight: bold");
		btnReset.setPrefSize(80, 25);
		btnReset.setStyle("-fx-background-color: #65737e;-fx-text-fill: #c0c5ce;-fx-font-weight: bold");
		btnTest.setStyle("-fx-background-color: #65737e;-fx-text-fill: #c0c5ce;-fx-font-weight: bold");
		
		addRow(1, labelNom, txtNom);
		addRow(2, labelPrenom, txtPrenom);
		addRow(3, labelDepartement, txtDepartement);
		addRow(4, labelPromotion, txtPromotion);
		addRow(5, labelAnnee, txtAnne);

		add(btnValider, 1, 6);
		add(btnReset, 1, 7);
		add(btnTest, 2, 8);

		setAlignment(Pos.CENTER);
		setVgap(20);
		setHgap(20);

		btnValider.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {

				// Récupérer les infos du stagiaire à ajouter 

				// crée un stagiaire 
				Stagiaire stagiaire = new Stagiaire(txtNom.getText(), txtPrenom.getText(), 
						txtDepartement.getText(), txtPromotion.getText(), txtAnne.getText());

				// valider les infos
				String validationResult = validate(stagiaire);

				if( validationResult.isEmpty() ) {
					fenetrePrincipale.ajouterStagiaire(stagiaire);
					stage.close();
				} else {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setHeaderText("Erreurs de saisie");
					alert.setContentText(validationResult);
					alert.show();
				}
			}
		});

		btnReset.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {

				txtNom.clear();
				txtPrenom.clear();
				txtDepartement.clear();
				txtPromotion.clear();
				txtAnne.clear();

			}
		});
		
		btnTest.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {

				txtNom.setText("TESTNOM");;
				txtPrenom.setText("TESTPRENOM");
				txtDepartement.setText("100");;
				txtPromotion.setText("TEST PROMO");
				txtAnne.setText("2021");

			}
		});


	}

	private String validate(Stagiaire stagiaire) {
		StringBuilder builder = new StringBuilder();

		if(stagiaire.getNom() == null || stagiaire.getNom().trim().isEmpty()) {
			builder.append("Nom erroné\n");
		}
		if(stagiaire.getPrenom() == null || stagiaire.getPrenom().trim().isEmpty()) {
			builder.append("Prénom erroné\n");
		}
		if(stagiaire.getDepartement() == null || stagiaire.getDepartement().trim().isEmpty()) {
			builder.append("Département erroné\n");
		}
		if(stagiaire.getPromotion() == null || stagiaire.getPromotion().trim().isEmpty()) {
			builder.append("Promotion erronée\n");
		}
		if(stagiaire.getAnnee() == null || stagiaire.getAnnee().trim().isEmpty()) {
			builder.append("Année erronée\n");
		}


		try {
			// on essaie de "parse" la chaine année en valeur numérique 
			// si ok => conversion
			Integer.parseInt(txtAnne.getText());

		} catch(NumberFormatException e) {
			// sinon ça donne une exception du coup on va ajouter l'erreur
			System.err.println(e.getMessage());
			builder.append("Année non numérique\n");
		}

		return builder.toString();
	}

	public void setStage(Stage stage2) {
		stage = stage2;
	}

	public void setFenetrePrincipale(FenetrePrincipale principale) {
		fenetrePrincipale = principale;
	}
}

