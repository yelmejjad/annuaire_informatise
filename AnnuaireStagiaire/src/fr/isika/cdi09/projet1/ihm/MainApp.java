package fr.isika.cdi09.projet1.ihm;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainApp extends Application{

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		// avant de lancer l'appli 
		
		// 1 - vérifier si un fichier binaire existe déjà (écrit déjà)
		
		// si non -> lire le fichier texte et créer l'arbre et le fichier binaire
		
		// Si oui -> lancer l'appli directement
		
		Login loginInterface = new Login();
		//FenetrePrincipale deuxiemeFenetre = new FenetrePrincipale();
		
		Scene scene = new Scene(loginInterface);
			
		primaryStage.setTitle("Login");
		primaryStage.setScene(scene);
		primaryStage.sizeToScene();
		//primaryStage.setResizable(false);
		primaryStage.show();
	}

	}


