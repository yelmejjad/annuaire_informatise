package fr.isika.cdi09.projet1.ihm;

import fr.isika.cdi09.projet1.objects.Stagiaire;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class InterfaceMettreAjour extends GridPane{
	private FenetrePrincipale fenetrePrincipale;
	private Stage stage;
private Stagiaire stagiaire;
	Label labelNom = new Label("Nom : ");
	TextField txtNom = new TextField();


	Label labelPrenom = new Label("Prénom : ");
	TextField txtPrenom = new TextField();

	Label labelDepartement = new Label("Département : ");
	TextField txtDepartement = new TextField();

	Label labelPromotion= new Label("Promotion : ");
	TextField txtPromotion = new TextField();

	Label labelAnnee = new Label("Année : ");
	TextField txtAnne = new TextField();

	Button btnMettreAjour = new Button("Mettre à jour");

	

	
	public InterfaceMettreAjour(FenetrePrincipale fenetrePrincipale) {
		this.fenetrePrincipale = fenetrePrincipale;
		txtNom.setDisable(true);
		
		setPrefSize(450,500);
		setStyle("-fx-background-color: #c0c5ce");
		
		btnMettreAjour.setAlignment(Pos.BOTTOM_CENTER);
		btnMettreAjour.setStyle("-fx-background-color: #65737e;-fx-text-fill: #c0c5ce;-fx-font-weight: bold");

		addRow(1, labelNom, txtNom);
		addRow(2, labelPrenom, txtPrenom);
		addRow(3, labelDepartement, txtDepartement);
		addRow(4, labelPromotion, txtPromotion);
		addRow(5, labelAnnee, txtAnne);
		add(btnMettreAjour, 1,6);

		setAlignment(Pos.CENTER);

		setVgap(20);
		setHgap(20);

		btnMettreAjour.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				//on instancie le nouveau pane
				/*
				// on crée une nouvelle scene avec le nouveau panneau
				Scene firstScene = new Scene(fenetrePrincipale);
				// on recupere le stage
				Stage stage = (Stage) InterfaceMettreAjour.this.getScene().getWindow();
				stage.setTitle("Annuaire");
				stage.setScene(firstScene);
				*/
				
				stagiaire.setPrenom(txtPrenom.getText());
				stagiaire.setDepartement(txtDepartement.getText());
				stagiaire.setPromotion(txtPromotion.getText());
				stagiaire.setAnnee(txtAnne.getText());
	

				// valider les infos
				String validationResult = validate(stagiaire);
				
				if( validationResult.isEmpty() ) {
					fenetrePrincipale.MiseAJourStagiaire(stagiaire);
					stage.close();
				} else {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setHeaderText("Erreurs de saisie");
					alert.setContentText(validationResult);
					alert.show();
				}
				
			}
		});

	}

	public void afficherInfosStagiaire(Stagiaire stagiaire) {
		txtNom.setText(stagiaire.getNom());
		txtPrenom.setText(stagiaire.getPrenom());
		txtDepartement.setText(stagiaire.getDepartement());
		txtPromotion.setText(stagiaire.getPromotion());
		txtAnne.setText(stagiaire.getAnnee());
		this.setStagiaire(stagiaire);
	}
	
	private String validate(Stagiaire stagiaire) {
		StringBuilder builder = new StringBuilder();
		
		if(stagiaire.getNom() == null || stagiaire.getNom().trim().isEmpty()) {
			builder.append("Nom erroné\n");
		}
		if(stagiaire.getPrenom() == null || stagiaire.getPrenom().trim().isEmpty()) {
			builder.append("Prénom erroné\n");
		}
		if(stagiaire.getDepartement() == null || stagiaire.getDepartement().trim().isEmpty()) {
			builder.append("Département erroné\n");
		}
		if(stagiaire.getPromotion() == null || stagiaire.getPromotion().trim().isEmpty()) {
			builder.append("Promotion erronée\n");
		}
		if(stagiaire.getAnnee() == null || stagiaire.getAnnee().trim().isEmpty()) {
			builder.append("Année erronée\n");
		}
		
		
		try {
			// on essaie de "parse" la chaine année en valeur numérique 
			// si ok => conversion
			Integer.parseInt(txtAnne.getText());
			
		} catch(NumberFormatException e) {
			// sinon ça donne une exception du coup on va ajouter l'erreur
			System.err.println(e.getMessage());
			builder.append("Année non numérique\n");
		}
		
		return builder.toString();
	}

	public FenetrePrincipale getFenetrePrincipale() {
		return fenetrePrincipale;
	}

	public void setFenetrePrincipale(FenetrePrincipale fenetrePrincipale) {
		this.fenetrePrincipale = fenetrePrincipale;
	}
	public void setStage(Stage stage2) {
		stage = stage2;
	}

	public Stagiaire getStagiaire() {
		return stagiaire;
	}

	public void setStagiaire(Stagiaire stagiaire) {
		this.stagiaire = stagiaire;
	}

}

