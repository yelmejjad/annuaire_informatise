package fr.isika.cdi09.projet1.ihm;

import java.util.Optional;

import fr.isika.cdi09.projet1.impressionpdf.ImpressionPDF;
import fr.isika.cdi09.projet1.objects.Stagiaire;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class FenetrePrincipale extends BorderPane {

	private StagiaireDao dao = new StagiaireDao();
	
	private HBox hbox = new HBox(300);
	private Label lblAs = new Label("Annuaire des Stagiaires ISIKA");
	private Label lblBonjour = new Label("Bonjour Mr (prenom/nom) admin");

	private Button btnSeDeconnecter = new Button("Se déconnecter");

	private TitledPane gridButton = new TitledPane();
	private VBox vbox = new VBox(10);
	
	private Tooltip toolBtnAjouter = new Tooltip("Ajouter un stagiaire");
	private Button btnAjouter = new Button("Ajouter");
	private Tooltip toolBtnMettreAJour = new Tooltip("Mettre à jour les données d'un stagiaire");
	private Button btnMettreAJour = new Button("Mettre A Jour");
	
	private Tooltip toolBtnSupprimer = new Tooltip("Supprimer le stagiaire");
	private Button btnSupprimer = new Button("Supprimer");
	private Tooltip toolBtnImprimer = new Tooltip("Impression");
	private Button btnImprimer = new Button("Imprimer");

	private TitledPane gridTitle = new TitledPane();
	private GridPane grid = new GridPane();
	private Label labelNom = new Label("Nom : ");
	private TextField txtNom = new TextField();

	private Label labelPrenom = new Label("Prénom : ");
	private TextField txtPrenom = new TextField();

	private Label labelDepartement = new Label("Département : ");
	private TextField txtDepartement = new TextField();
	private Label labelPromotion = new Label("Promotion : ");
	private TextField txtPromotion = new TextField();
	private Label labelAnnee = new Label("Année : ");
	private TextField txtAnne = new TextField();
	private Button btnRechercher = new Button("Rechercher");
	private Button btnReset = new Button("Reset");
	
	private HBox hboxCR = new HBox();
	private Label copyRight = new Label("©Isika 2021");

	private TableView<Stagiaire> tableView;
	private InterfaceAjouter ajoutPane;
	private InterfaceMettreAjour majPane;

	public FenetrePrincipale() {
		setPrefSize(1400, 800);
		ObservableList<Stagiaire> observableStagiaires = FXCollections
				.observableArrayList(dao.getStagiairesFromArbre());
		tableView = new TableView<Stagiaire>(observableStagiaires);

		TableColumn<Stagiaire, String> colNom = new TableColumn<>("Nom");
		colNom.setCellValueFactory(new PropertyValueFactory<>("nom"));
		colNom.setStyle("-fx-alignment: CENTER-LEFT;");

		TableColumn<Stagiaire, String> colPrenom = new TableColumn<>("Prénom");
		colPrenom.setCellValueFactory(new PropertyValueFactory<>("prenom"));
		colPrenom.setStyle("-fx-alignment: CENTER-LEFT;");

		TableColumn<Stagiaire, String> colDepartement = new TableColumn<>("Département");
		colDepartement.setCellValueFactory(new PropertyValueFactory<>("departement"));
		colDepartement.setStyle("-fx-alignment: CENTER;");

		TableColumn<Stagiaire, String> colPromotion = new TableColumn<>("Promotion");
		colPromotion.setCellValueFactory(new PropertyValueFactory<>("promotion"));
		colPromotion.setStyle("-fx-alignment: CENTER-LEFT;");

		TableColumn<Stagiaire, String> colAnnee = new TableColumn<>("Année");
		colAnnee.setCellValueFactory(new PropertyValueFactory<>("annee"));
		colAnnee.setStyle("-fx-alignment: CENTER;");

		tableView.getColumns().addAll(colNom, colPrenom, colPromotion, colDepartement, colAnnee);
		tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		

		lblBonjour.setTextFill(Color.ORANGE);
		lblBonjour.setPadding(new Insets(10));
		lblBonjour.setStyle("-fx-font-size:15px");
		lblBonjour.setAlignment(Pos.BASELINE_CENTER);
		
		
		btnAjouter.setPrefSize(150, 40);
		btnAjouter.setTooltip(toolBtnAjouter);
		btnAjouter.setStyle("-fx-background-color: #65737e;-fx-text-fill: #c0c5ce;-fx-font-weight: bold");
		btnMettreAJour.setPrefSize(150, 40);
		btnMettreAJour.setTooltip(toolBtnMettreAJour);
		btnMettreAJour.setStyle("-fx-background-color: #65737e;-fx-text-fill: #c0c5ce;-fx-font-weight: bold");
		btnSupprimer.setPrefSize(150, 40);
		btnSupprimer.setTooltip(toolBtnSupprimer);
		btnSupprimer.setStyle("-fx-background-color: #65737e;-fx-text-fill: #c0c5ce;-fx-font-weight: bold");
		btnImprimer.setPrefSize(150, 40);
		btnImprimer.setTooltip(toolBtnImprimer);
		btnImprimer.setStyle("-fx-background-color: #65737e;-fx-text-fill: #c0c5ce;-fx-font-weight: bold");
		
		btnSeDeconnecter.setStyle("-fx-background-color: #65737e;-fx-text-fill: #c0c5ce;-fx-font-weight: bold");
		lblAs.setStyle("-fx-font-weight: bold");
		hbox.getChildren().addAll(lblAs, lblBonjour, btnSeDeconnecter);
		hbox.setAlignment(Pos.CENTER);
		hbox.setPrefSize(600, 100);
		
		vbox.getChildren().addAll(btnAjouter, btnMettreAJour, btnSupprimer, btnImprimer);
		vbox.setAlignment(Pos.CENTER);
		vbox.setPrefSize(200, 400);
		vbox.setStyle("-fx-background-color: #c0c5ce;-fx-text-fill: #c0c5ce;-fx-font-weight: bold");
	
		gridButton.setContent(vbox);
		gridButton.setText("Actions");
		gridButton.setStyle("-fx-font-weight: bold");
		gridButton.setAlignment(Pos.CENTER);
		
		btnRechercher.setPrefSize(100, 25);
		btnRechercher.setStyle("-fx-background-color: #65737e;-fx-text-fill: #c0c5ce;-fx-font-weight: bold");
		btnReset.setPrefSize(100, 25);
		btnReset.setStyle("-fx-background-color: #65737e;-fx-text-fill: #c0c5ce;-fx-font-weight: bold");
		grid.setPrefSize(400, 400);
		grid.addRow(1, labelNom, txtNom);
		grid.addRow(2, labelPrenom, txtPrenom);
		grid.addRow(3, labelDepartement, txtDepartement);
		grid.addRow(4, labelPromotion, txtPromotion);
		grid.addRow(5, labelAnnee, txtAnne);
		grid.add(btnRechercher, 1, 6);
		grid.add(btnReset, 1, 7);
		grid.setHgap(20);
		grid.setVgap(10);
		grid.setStyle("-fx-background-color: #c0c5ce;-fx-text-fill: #c0c5ce;-fx-font-weight: bold");
		grid.setAlignment(Pos.CENTER);
		gridTitle.setText("Recherche Avancée");
		gridTitle.setStyle("-fx-font-weight: bold");
		gridTitle.setContent(grid);
		gridTitle.setAlignment(Pos.CENTER);
		gridTitle.setExpanded(false);
		
		copyRight.setStyle("-fx-font-weight: bold");
		hboxCR.getChildren().add(copyRight);
	    hboxCR.setAlignment(Pos.BASELINE_RIGHT);    
		
		setStyle("-fx-background-color: #a7adba ");
		setLeft(gridTitle);
		setCenter(tableView);
		setMargin(tableView, new Insets(0, 20, 20, 20));
		setTop(hbox);
		setRight(gridButton);
		setBottom(hboxCR);
		this.setPadding(new Insets(10,10,10,10));

		btnSeDeconnecter.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
			
				Login firstPane = new Login();
				
				Scene firstScene = new Scene(firstPane);
		
				Stage stage = (Stage) FenetrePrincipale.this.getScene().getWindow();
				stage.setTitle("Login");
				stage.setScene(firstScene);
			}
		});

		btnSupprimer.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
			

				// message
				Stagiaire s = tableView.getSelectionModel().getSelectedItem();
				if (s != null) {
					Alert alert = new Alert(AlertType.CONFIRMATION);
					alert.setHeaderText("Confirmation suppression");
					alert.setContentText("Voulez vous supprimer ce stagiaire sélectionné ?\n" + s.getNom() + " " +s.getPrenom() );
					Optional<ButtonType> result =  alert.showAndWait();    
					if ( result.get().equals(ButtonType.OK) == true ){
						SupprimerStagiaire(s);}
				} else {
					Alert alert = new Alert(AlertType.WARNING);
					alert.setHeaderText("Avertissement sélection");
					alert.setContentText("Veuillez sélectionner un élement dans le tableau !");
					alert.show();
				}
			}
		});

		btnMettreAJour.setOnAction(new ModifierEventHandler(this));

		btnAjouter.setOnAction(new AjoutEventHandler(this));

		btnReset.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				txtNom.clear();
				txtPrenom.clear();
				txtDepartement.clear();
				txtPromotion.clear();
				txtAnne.clear();
				tableView.setItems(FXCollections.observableArrayList(dao.getStagiairesFromArbre()));

			}
		});

		btnImprimer.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {

				ImpressionPDF.imprimer(tableView.getItems());

			}
		});

		btnRechercher.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {

				rechercherStagiaire();

			}
		});
	
	
	}

	public void ajouterStagiaire(Stagiaire stagiaire) {
		this.dao.ajouterStagiaire(stagiaire);
		rechercherStagiaire();//tableView.setItems(FXCollections.observableArrayList(dao.getStagiairesFromArbre()));
	}
	
	public void MiseAJourStagiaire(Stagiaire stagiaire) {
		this.dao.MiseAJourStagiaire(stagiaire);
		rechercherStagiaire();//tableView.setItems(FXCollections.observableArrayList(dao.getStagiairesFromArbre()));
	}
	public void SupprimerStagiaire(Stagiaire stagiaire) {
		this.dao.SupprimerStagiaire(stagiaire);
		rechercherStagiaire();//tableView.setItems(FXCollections.observableArrayList(dao.getStagiairesFromArbre()));
	}

	public void rechercherStagiaire() {
		// nom = txtNom.getText();
		// this.dao.rechercherStagiaire(nom);
		tableView.setItems(FXCollections.observableArrayList(dao.getStagiairesFromArbre())
				.filtered(s -> s.getNom().toUpperCase().contains(txtNom.getText().toUpperCase()) 
						&& s.getAnnee().contains(txtAnne.getText())
						&& s.getPrenom().toUpperCase().contains(txtPrenom.getText().toUpperCase())
						&& s.getPromotion().contains(txtPromotion.getText())
						&& s.getDepartement().contains(txtDepartement.getText())));
		tableView.refresh();
	}

	public Button getBtnMettreAJour() {
		return btnMettreAJour;
	}

	public void setBtnMettreAJour(Button btnMettreAJour) {
		this.btnMettreAJour = btnMettreAJour;
	}

	public Button getBtnSupprimer() {
		return btnSupprimer;
	}

	public void setBtnSupprimer(Button btnSupprimer) {
		this.btnSupprimer = btnSupprimer;
	}

	public HBox getHbox() {
		return hbox;
	}

	public void setHbox(HBox hbox) {
		this.hbox = hbox;
	}

	public VBox getVbox() {
		return vbox;
	}

	public void setVbox(VBox vbox) {
		this.vbox = vbox;
	}

	public Label getLblBonjour() {
		return lblBonjour;
	}

	public void setLblBonjour(Label lblBonjour) {
		

	}

	class AjoutEventHandler implements EventHandler<ActionEvent> {

		private FenetrePrincipale principale;

		public AjoutEventHandler(FenetrePrincipale principale) {
			this.principale = principale;
		}

		@Override
		public void handle(ActionEvent event) {
			ajoutPane = new InterfaceAjouter();
			Scene firstScene = new Scene(ajoutPane);
			Stage stage = new Stage();
			stage.setTitle("Ajout");
			stage.setScene(firstScene);
			ajoutPane.setStage(stage);
			ajoutPane.setFenetrePrincipale(principale);
			stage.show();
		}
	}

	class ModifierEventHandler implements EventHandler<ActionEvent> {

		private FenetrePrincipale principale;

		public ModifierEventHandler(FenetrePrincipale principale) {
			this.principale = principale;
		}

		@Override
		public void handle(ActionEvent event) {
			Stagiaire s = tableView.getSelectionModel().getSelectedItem();
			if(s == null) {
				Alert alert = new Alert(AlertType.WARNING);
				alert.setHeaderText("Erreur Sélection");
				alert.setContentText("Aucun stagiaire n'a été sélectionné !");
				alert.show();
			} else {
				majPane = new InterfaceMettreAjour(principale);
				Scene firstScene = new Scene(majPane);
				Stage stage = new Stage();
				stage.setTitle("Mise à jour");
				stage.setScene(firstScene);
				majPane.setStage(stage);
				majPane.afficherInfosStagiaire(s);
				stage.show();
			}
		}

	}

}
