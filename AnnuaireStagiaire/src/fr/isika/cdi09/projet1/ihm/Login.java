package fr.isika.cdi09.projet1.ihm;


import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;


public class Login extends BorderPane {

	private GridPane grid = new GridPane();

	private Label lblBienvenu = new Label("Bienvenu dans l’annuaire des Stagiaires Isika, veuillez vous connecter");

	private Label lblNomCompte = new Label("Nom de compte");
	private Tooltip toolTxtNomCompte = new Tooltip("Veuillez saisir votre identifiant");
	private TextField txtNomCompte = new TextField();

	private Label lblMotdePasse = new Label("Mot de Passe");
	private Tooltip toolTxtMotdePasse = new Tooltip("Veuillez saisir votre mot de passe");
	private TextField txtMotdePasse = new PasswordField();
	private TextField afficheMDP = new TextField();
	private CheckBox checkBox = new CheckBox("Montrer le mot de passe");


	private VBox vboxConnexion = new VBox(20);
	private Button btnConnexion = new Button("Connexion");
	private Label etatConnexion = new Label("");

	public Login() {

		setStyle("-fx-background-color: #a7adba ");
		lblBienvenu.setStyle("-fx-font: 16 arial; -fx-font-weight: bold");
		lblNomCompte.setStyle("-fx-font-weight: bold");
		lblMotdePasse.setStyle("-fx-font-weight: bold");

		txtNomCompte.setTooltip(toolTxtNomCompte);
		txtMotdePasse.setTooltip(toolTxtMotdePasse);

		checkBox.setStyle("-fx-font-weight: bold");

		grid.setPrefSize(800,150);
		grid.add(lblBienvenu, 1, 2);

		grid.addRow(4, lblNomCompte, txtNomCompte);
		grid.addRow(5, lblMotdePasse, txtMotdePasse);
		grid.add(afficheMDP, 1, 5);
		grid.add(checkBox, 1, 6);

		grid.setAlignment(Pos.CENTER);

		afficheMDP.setManaged(false);
		afficheMDP.setVisible(false);
		afficheMDP.managedProperty().bind(checkBox.selectedProperty());
		afficheMDP.visibleProperty().bind(checkBox.selectedProperty());

		txtMotdePasse.managedProperty().bind(checkBox.selectedProperty().not());
		txtMotdePasse.visibleProperty().bind(checkBox.selectedProperty().not());

		afficheMDP.textProperty().bindBidirectional(txtMotdePasse.textProperty());

		grid.setHgap(50);
		grid.setVgap(20);

		btnConnexion.setStyle("-fx-background-color: #65737e;-fx-text-fill: #c0c5ce;-fx-font-weight: bold");
		vboxConnexion.setPrefSize(800,100);
		vboxConnexion.getChildren().addAll(etatConnexion,btnConnexion);
		vboxConnexion.setAlignment(Pos.CENTER);

		setCenter(grid);
		setBottom(vboxConnexion);


		btnConnexion.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {

				start();
			}
		});

		txtMotdePasse.setOnKeyReleased(new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent ke) {
				if (ke.getCode() == KeyCode.ENTER)
				{
					start();
				}		    
			}
		});

	}

	public void start() {
		if(txtNomCompte.getText().equals("employee01") && txtMotdePasse.getText().equals("employee01")) {
			FenetrePrincipale firstPane = new FenetrePrincipale();
			Scene firstScene = new Scene(firstPane);

			firstPane.getBtnMettreAJour().setVisible(false);
			firstPane.getBtnSupprimer().setVisible(false);
			firstPane.getLblBonjour().setText("Bonjour Employé N 01");
			firstPane.getLblBonjour().setStyle("-fx-font: 24 arial;-fx-font-weight: bold");
			firstPane.getLblBonjour().setTextFill(Color.web("#343d46"));

			Stage stage = (Stage) Login.this.getScene().getWindow();

			stage.setTitle("Annuaire");
			stage.setScene(firstScene);
		} else {
			etatConnexion.setText("Nom de compte ou mot de passe erroné, veuillez réessayer");
			etatConnexion.setStyle("-fx-text-fill:  red");
		}


		if(txtNomCompte.getText().equals("admin") && txtMotdePasse.getText().equals("admin")) {

			FenetrePrincipale firstPane = new FenetrePrincipale();

			Scene firstScene = new Scene(firstPane);

			Stage stage = (Stage) Login.this.getScene().getWindow();
			firstPane.getLblBonjour().setText("Bonjour ADMIN");
			firstPane.getLblBonjour().setStyle("-fx-font: 24 arial;-fx-font-weight: bold");
			firstPane.getLblBonjour().setTextFill(Color.web("#343d46"));
			stage.setTitle("Annuaire");
			stage.setScene(firstScene);
		} else {
			etatConnexion.setText("Nom de compte ou mot de passe erroné, veuillez réessayer");
			etatConnexion.setStyle("-fx-text-fill:  red");
		}

	}



}
