package fr.isika.cdi09.projet1.ihm;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class InterfaceRechercheAvancee extends GridPane {

	Label labelNom = new Label("Nom : ");
	TextField txtNom = new TextField();

	Label labelPrenom = new Label("Prénom : ");
	TextField txtPrenom = new TextField();

	Label labelDepartement = new Label("Département : ");
	TextField txtDepartement = new TextField();

	Label labelPromotion= new Label("Promotion : ");
	TextField txtPromotion = new TextField();

	Label labelAnnee = new Label("Année : ");
	TextField txtAnne = new TextField();

	Button btnRechercher = new Button("Rechercher");

	public InterfaceRechercheAvancee() {

		setPrefSize(450,500);

		btnRechercher.setAlignment(Pos.BOTTOM_CENTER);

		addRow(1, labelNom, txtNom);
		addRow(2, labelPrenom, txtPrenom);
		addRow(3, labelDepartement, txtDepartement);
		addRow(4, labelPromotion, txtPromotion);
		addRow(5, labelAnnee, txtAnne);
		add(btnRechercher, 1,6);

		setAlignment(Pos.CENTER);

		setVgap(20);
		setHgap(20);
		
		btnRechercher.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				//on instancie le nouveau pane
				FenetrePrincipale firstPane = new FenetrePrincipale();
				// on crée une nouvelle scene avec le nouveau panneau
				Scene firstScene = new Scene(firstPane);
				// on recupere le stage
				Stage stage = (Stage) InterfaceRechercheAvancee.this.getScene().getWindow();
				stage.setTitle("Annuaire");
				stage.setScene(firstScene);
			}
		});

	}
}
