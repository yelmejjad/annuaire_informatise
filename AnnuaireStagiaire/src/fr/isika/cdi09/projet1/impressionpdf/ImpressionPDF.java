package fr.isika.cdi09.projet1.impressionpdf;

import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.GrayColor;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import fr.isika.cdi09.projet1.objects.Stagiaire;

public class ImpressionPDF {

	private static String filePath = "export.pdf";

	public static void imprimer(List<Stagiaire> lesStagiaires) {

		try {
			FileOutputStream stream = new FileOutputStream(filePath);

			Document document = new Document(PageSize.A4, 50, 50, 30, 20);
			PdfWriter writer = PdfWriter.getInstance(document, stream);

			Paragraph bv = new Paragraph("Liste des stagiaires ISIKA",
			new Font(FontFamily.HELVETICA, 14, Font.BOLD, GrayColor.ORANGE));
			bv.setAlignment(Element.ALIGN_CENTER);
			bv.setSpacingAfter(40f);

			Paragraph tb = new Paragraph("total de stagiaire à imprimer: " + lesStagiaires.size());
			tb.setAlignment(Element.ALIGN_CENTER);
			tb.setSpacingAfter(40f);
			
			new Font(FontFamily.HELVETICA, 8, Font.BOLDITALIC, GrayColor.BLACK);
			Font font = new Font(FontFamily.HELVETICA, 10, Font.NORMAL, GrayColor.GRAYBLACK);
			Font fh = new Font(FontFamily.HELVETICA, 10, Font.BOLD, GrayColor.GRAYBLACK);

			document.open();

			document.add(new Paragraph(bv));

			PdfPTable table = new PdfPTable(5);
			table.setWidths(new int[] { 100, 100, 50, 60, 50 });

			PdfPCell pdfCellheader = new PdfPCell();
			pdfCellheader.setHorizontalAlignment(Element.ALIGN_CENTER);
			pdfCellheader.setVerticalAlignment(Element.ALIGN_CENTER);
			pdfCellheader.setBackgroundColor(GrayColor.GRAY);

			Phrase nom = new Phrase("Nom", fh);
			pdfCellheader.setPhrase(nom);
			table.addCell(pdfCellheader);

			Phrase prenom = new Phrase("Prénom", fh);
			pdfCellheader.setPhrase(prenom);
			table.addCell(pdfCellheader);
			Phrase dep = new Phrase("Dep.", fh);
			pdfCellheader.setPhrase(dep);
			table.addCell(pdfCellheader);
			Phrase promo = new Phrase("Promo", fh);
			pdfCellheader.setPhrase(promo);
			table.addCell(pdfCellheader);
			Phrase annee = new Phrase("Année", fh);
			pdfCellheader.setPhrase(annee);
			table.addCell(pdfCellheader);

			PdfPCell pdfCelltxt = new PdfPCell();
			pdfCelltxt.setHorizontalAlignment(Element.ALIGN_LEFT);
			pdfCelltxt.setVerticalAlignment(Element.ALIGN_MIDDLE);
			pdfCelltxt.setBackgroundColor(new BaseColor(0xdd7e6b));

			PdfPCell pdfCellnum = new PdfPCell();
			pdfCellnum.setHorizontalAlignment(Element.ALIGN_CENTER);
			pdfCellnum.setVerticalAlignment(Element.ALIGN_MIDDLE);
			pdfCellnum.setBackgroundColor(new BaseColor(0xdd7e6b));

			for (Stagiaire stagiaire : lesStagiaires) {
				Phrase p = new Phrase(stagiaire.getNom(), font);
				pdfCelltxt.setPhrase(p);
				table.addCell(pdfCelltxt);
				Phrase p1 = new Phrase(stagiaire.getPrenom(), font);
				pdfCelltxt.setPhrase(p1);
				table.addCell(pdfCelltxt);
				Phrase p2 = new Phrase(stagiaire.getDepartement(), font);
				pdfCellnum.setPhrase(p2);
				table.addCell(pdfCellnum);
				Phrase p3 = new Phrase(stagiaire.getPromotion(), font);
				pdfCelltxt.setPhrase(p3);
				table.addCell(pdfCelltxt);
				Phrase p4 = new Phrase(stagiaire.getAnnee(), font);
				pdfCellnum.setPhrase(p4);
				table.addCell(pdfCellnum);
			}

			document.add(table);
			document.add(new Paragraph(tb));

			document.close();
			stream.close();

			
			open(new File(filePath));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void open(File pdfFile) {
		try {
			if (pdfFile.exists()) {

				if (Desktop.isDesktopSupported()) {
					Desktop.getDesktop().open(pdfFile);
				} else {
					System.err.println("Awt Desktop is not supported!");
				}
			} else {
				System.err.println("File is not exists!");
			}
			System.out.println("Ouverture du fichier .. ");
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}
}