package fr.isika.cdi09.projet1.objects;
/*
 * 1. 把文件地址给予fichierBin，
 * 2. ligne 12: comment traduire?
 * 3. try catch ici 是固定用法。
 * 4. stringbuilder 设定每个champs的长度。
 * 5. ligne 33: raf ecrit les noms + prénoms + 左右娃的index。
 * 6. 这个类的存在目的是啥到底？？？
 */
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

public class EcritureBinaire {

	public void ecrireNouveauNoeud(NoeudStagiaire st, String filePath) {
		File fichierBin = new File(filePath); 
		try (RandomAccessFile raf = new RandomAccessFile(fichierBin, "rw");) {
			raf.seek(raf.length());
			st.getVal().setIndice((raf.length())/166);  
			
			StringBuilder SB = new StringBuilder();
			SB.append(st.getVal().getNom());
			SB.setLength(20);
			SB.append(st.getVal().getPrenom());
			SB.setLength(40);
			SB.append(st.getVal().getDepartement());
			SB.setLength(50);
			SB.append(st.getVal().getPromotion());
			SB.setLength(65);
			SB.append(st.getVal().getAnnee());
			SB.setLength(75);
					
			raf.writeChars(SB.toString());
			raf.writeLong(st.getGauche());
			raf.writeLong(st.getDroite());
			
			

		} catch (IOException ioe) {
			System.err.println(ioe.getMessage());
		}
		}
		
		public void MiseAJourNoeud(NoeudStagiaire st, String filePath) {
			File fichierBin = new File(filePath);
			try { RandomAccessFile raf = new RandomAccessFile(fichierBin, "rw");
				
				Long position = (st.getVal().getIndice()*166) ;  
				raf.seek(position);
				
				StringBuilder SB = new StringBuilder();
				SB.append(st.getVal().getNom());
				SB.setLength(20);
				SB.append(st.getVal().getPrenom());
				SB.setLength(40);
				SB.append(st.getVal().getDepartement());
				SB.setLength(50);
				SB.append(st.getVal().getPromotion());
				SB.setLength(65);
				SB.append(st.getVal().getAnnee());
				SB.setLength(75);
						
				raf.writeChars(SB.toString());
				raf.writeLong(st.getGauche());
				raf.writeLong(st.getDroite());

			} catch (IOException ioe) {
				System.err.println(ioe.getMessage());
			}
	}
		public void SuppressionNoeud(NoeudStagiaire st, String filePath) {
			File fichierBin = new File(filePath);
			try { RandomAccessFile raf = new RandomAccessFile(fichierBin, "rw");
				
				Long position = (st.getVal().getIndice()*166) ;  
				raf.seek(position);
				
				StringBuilder SB = new StringBuilder();
				SB.append("");
				SB.setLength(150); // TODO  a la lecture, eviter les stagiaires vides
						
				raf.writeChars(SB.toString());
				raf.writeLong(0);
				raf.writeLong(0);

			} catch (IOException ioe) {
				System.err.println(ioe.getMessage());
			}
	}

}
