package fr.isika.cdi09.projet1.objects;

/*
 * 1. extraitre les données de bin ou dans bin?

 * 2. besoin de la liste des sta(chaque noeud);
 * 3. try catch
 * 4. raf que lire;
 * 5. raf cherche  
 * 6. 我还是没明白这个类是为啥？ elle lit le fichier bin et me rend l'arbre; 
 */
import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class ExtractDataFileBin {

	public ArbreStagiaire extract(String fileName) {  // 什么东西来的？
		File fichierBinaire = new File(fileName);  // 建立新文件
		List<NoeudStagiaire> lesNoeudStagiaires = new ArrayList<>();  // sta清单排队来等着被用。
		
		try {
			RandomAccessFile raf = new RandomAccessFile(fichierBinaire, "r"); // 固定写法
			raf.seek(0); // 找啥？
			long compteur = 0;
			int index = 0;
			Stagiaire s = new Stagiaire();
			Boolean Abandon = false;  // 放弃不是真的：不放弃。
			
			while (Abandon == false && compteur < raf.length()) {
				try {
					// 以下这6行，干啥？
					String lectureNom = "";
					String lecturePrenom = "";
					String lectureDepartement = "";
					String lecturePromotion = "";
					String lectureAnnee = "";
					long lectureGauche = -1;
					long lectureDroite = -1;
					
					for (int i = 0; i < 20; i++) {
						lectureNom += raf.readChar();
					}
					s.setNom(lectureNom.trim());
					compteur += 40;

					for (int i = 0; i < 20; i++) {
						lecturePrenom += raf.readChar();
					}
					s.setPrenom(lecturePrenom.trim());
					compteur += 40;
					
					for (int i = 0; i < 10; i++) {
						lectureDepartement += raf.readChar();
					}
					s.setDepartement(lectureDepartement.trim());
					compteur += 20;

					for (int i = 0; i < 15; i++) {
						lecturePromotion += raf.readChar();
					}
					s.setPromotion(lecturePromotion.trim());

					compteur += 30;

					for (int i = 0; i < 10; i++) {
						lectureAnnee += raf.readChar();
					}
					s.setAnnee(lectureAnnee.trim());

					s.setIndice(index);
					compteur += 20;

					lectureGauche = raf.readLong();
					lectureDroite = raf.readLong();

					compteur += 16; // 把所有的champs都读了一遍， 左右娃也没放过，然后呢？
					
					if (lectureNom.isEmpty() == false && (int)lectureNom.charAt(0) != 0) { //如果名字不为空，且首位index不为零。
					NoeudStagiaire noeud = new NoeudStagiaire(s, lectureGauche, lectureDroite); // 那么新节点就包含了sta和它的左右娃。
					lesNoeudStagiaires.add(noeud); // 并且把新节点加到树上。
					s = new Stagiaire();  // 为啥declaration在后面？
					noeud = new NoeudStagiaire(); // 同上。
					}
					index++;
					
				} catch (EOFException e) {
					System.err.println(e.getMessage());
					Alert a = new Alert(AlertType.ERROR, "ERREUR à la lecture du fichier BIN");
					Abandon = true;
				}
			}
			raf.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}

		// ça doit être renvoyé par la méthode;
		ArbreStagiaire arbre = new ArbreStagiaire();  // creer  nouvel arbre;
		lesNoeudStagiaires.forEach(st -> {   // ajouter chaque sta(noeud) dans l'arbre; utilisation de lambda: ligne 106/107; 
			arbre.ajouterNoeud(st);
		});

		return arbre;
	}

}
