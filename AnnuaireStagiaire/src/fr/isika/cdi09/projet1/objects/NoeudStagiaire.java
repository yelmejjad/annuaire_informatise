package fr.isika.cdi09.projet1.objects;

/* 1. definir 6 variables(si bcp); 左右龙，noeud courant，左右娃，双胞胎。
 * 2. 空构造
 * 3. 有料构造
 * 4. this
 */
import java.util.ArrayList;
import java.util.List;

public class NoeudStagiaire {

	private long droite = 0;
	private long gauche = 0;
	private Stagiaire val;
	private NoeudStagiaire filsGauche = null;
	private NoeudStagiaire filsDroit = null;
	private List<NoeudStagiaire> doublons = new ArrayList<>();
     // list 的名字叫doublons，里面装的全都是noeudSta。

	public NoeudStagiaire() {

	}
	
	public NoeudStagiaire(Stagiaire val) {
		
		this.val = val;
	}
	
	public NoeudStagiaire( Stagiaire val,  long gauche, long droite) {
		this.val = val;
		this.gauche = gauche;
		this.droite = droite;
	}
	
	public void ajouterDoublon(NoeudStagiaire noeudStagiaire) {
		
		this.doublons.add(noeudStagiaire);
		
	}


	@Override
	public String toString() {
		return "NoeudStagiaire [droite=" + droite + ", gauche=" + gauche + ", Nom=" + val.getNom().trim()+ "]";
	}



	public long getDroite() {
		return droite;
	}


	public void setDroite(long droite) {
		this.droite = droite;
	}


	public long getGauche() {
		return gauche;
	}


	public void setGauche(long gauche) {
		this.gauche = gauche;
	}



	public Stagiaire getVal() {
		return val;
	}


	public void setVal(Stagiaire val) {
		this.val = val;
	}


	public NoeudStagiaire getFilsGauche() {
		return filsGauche;
	}


	public void setFilsGauche(NoeudStagiaire filsGauche) {
		this.filsGauche = filsGauche;
	}


	public NoeudStagiaire getFilsDroit() {
		return filsDroit;
	}


	public void setFilsDroit(NoeudStagiaire filsDroit) {
		this.filsDroit = filsDroit;
	}

}


	
	