package fr.isika.cdi09.projet1.objects;


public class Stagiaire implements Comparable {

	/* 1. définir les 5 champs + indice
	 * 2. constructeur vide
	 * 3. constructeur rempli: this. afin d'être utilisé ailleurs;
	 * 4. override (j'ai pas compris le but); 
	 * 5. getter setter
	 * 6. compare to is for what?
	 */
	private String nom;
	private String prenom;
	private String departement;
	private String prom;
	private String annee;
	private long indice;
	
	public Stagiaire() {

	}

	public Stagiaire(String nom, String prenom, String departement, String promotion, String annee) {
		this.nom = nom;
		this.prenom = prenom;
		this.departement = departement;
		this.prom = promotion;
		this.annee = annee;
	}
	
	
	
	public Stagiaire(String nom) {
		this.nom = nom;
	}

	@Override
	public String toString() {
		return "Stagiaire [nom=" + nom + ", prenom=" + prenom + ", departement=" + departement + ", promotion="
				+ prom + ", annee=" + annee + "]";
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getDepartement() {
		return departement;
	}

	public void setDepartement(String departement) {
		this.departement = departement;
	}

	public String getPromotion() {
		return prom;
	}

	public void setPromotion(String promotion) {
		this.prom = promotion;
	}

	public String getAnnee() {
		return annee;
	}

	public void setAnnee(String annee) {
		this.annee = annee;
	}


	public long getIndice() {
		return indice;
	}

	public void setIndice(long indice) {
		this.indice = indice;
	}

	public int compareTo2(Object o) {
		if(o.getClass() != this.getClass())
		return 0;
		Stagiaire other = (Stagiaire)o;
		return (this.nom ).compareTo(other.nom );
	}
	public int compareTo(Object o) {
		if(o.getClass() != this.getClass())
		return 0;
		Stagiaire other = (Stagiaire)o;
		return (this.nom + this.prenom + this.annee + this.departement + this.prom).compareTo(other.nom + other.prenom+ other.annee + other.departement + other.prom);
	}


	

}
