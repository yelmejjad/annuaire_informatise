package fr.isika.cdi09.projet1.objects;

/*  1. ici on va crée une liste, et on va la positionner dans le File f;
 *  2. try catch pour capter les exceptions et les afficher dans le console; 
 *  3. BufferedReader pour lire txt; 
 *  4. StringBuiler pour 串联 les 5 champs dans une ligne;
 *  5. return une liste des stagiaires; 
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class LectureFichierStagiaire {
	
	public static List<Stagiaire> lectureFichierStagiaire(File f){
		
		List<Stagiaire> stagiaireListe = new ArrayList<>();

		try(BufferedReader br = new BufferedReader(new FileReader(f))){

			int Numligne = 0;
			long index = 0;
			String line;
			Stagiaire s = new Stagiaire();
			StringBuilder SB = new StringBuilder();
			while( (line = br.readLine()) != null) {

				Numligne++;
				

				if (Numligne == 1) {

					SB = new StringBuilder();
					SB.append(line.toUpperCase());
					SB.setLength(20);
					s.setNom(SB.toString());
				}

				if (Numligne == 2) {
					
					SB = new StringBuilder();
					SB.append(line.toUpperCase());
					SB.setLength(20);
					s.setPrenom(SB.toString());
				}
				
				if (Numligne == 3) {

					SB = new StringBuilder();
					SB.append(line.toUpperCase());
					SB.setLength(10);
					s.setDepartement(SB.toString());
				}

				if (Numligne == 4) {
					
					SB = new StringBuilder();
					SB.append(line.toUpperCase());
					SB.setLength(15);
					s.setPromotion(SB.toString());
				}

				if (Numligne == 5)
				{
					SB = new StringBuilder();
					SB.append(line.toUpperCase());
					SB.setLength(10);
					s.setAnnee(SB.toString());


				}

				if (Numligne == 6 )
				{	
					s.setIndice(index);
					index++;
					stagiaireListe.add(s);
					Numligne = 0;
					s = new Stagiaire();

				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("Can't find file " + f.toString());
		} catch (IOException e) {
			System.out.println("Unable to read file " + f.toString());
		}	
		
		return stagiaireListe;
	}

}
