package fr.isika.cdi09.projet1.objects;

/*
 * 1. définir la racine;
 * 2. 
 * 3. les 4 méthodes de: ajouter, afficher, supprimer, modifier;
 * 4. 
 */

import java.util.List;

public class ArbreStagiaire {

	private NoeudStagiaire racine;


	public NoeudStagiaire getRacine() {
		return racine;
	}


	public ArbreStagiaire() {
	}

	public ArbreStagiaire(NoeudStagiaire racine) {
		this.racine = racine;
	}


	public void ajouterNoeud(NoeudStagiaire noeudStagiaire) {

		NoeudStagiaire courant = racine;
		if (courant == null) {
			racine = new NoeudStagiaire(noeudStagiaire.getVal());
			return;
		} else {

			boolean trouve = false;
			while (!trouve) {

				int test = noeudStagiaire.getVal().compareTo(courant.getVal());

				if (test == 0) {
					int testStrict = noeudStagiaire.getVal().compareTo2(courant.getVal());
					if (test == 0) {
						trouve = true;
					}
					else
					{
						// TODO faire la gestion des faux doublons ???
					}

				} else if (test < 0) {
					if (courant.getFilsGauche() == null) {

						courant.setFilsGauche(new NoeudStagiaire(noeudStagiaire.getVal()));
						courant.setGauche(noeudStagiaire.getVal().getIndice());
						trouve = true;
					} else {
						courant = courant.getFilsGauche();
					}
				} else {
					if(courant.getFilsDroit() == null) {

						courant.setFilsDroit(new NoeudStagiaire(noeudStagiaire.getVal()));
						courant.setDroite(noeudStagiaire.getVal().getIndice());
						trouve = true;
					} else {

						courant = courant.getFilsDroit();
					}
				} 

			}
		}
	}


	public void parcousInfixe1(NoeudStagiaire root, List<Stagiaire> stagiaires) {

		if( root != null) {

			parcousInfixe1(root.getFilsGauche(), stagiaires);
			stagiaires.add(root.getVal());
			parcousInfixe1(root.getFilsDroit(), stagiaires);

		}
	}

	public void parcousInfixe2(NoeudStagiaire root, List<NoeudStagiaire> stagiaires) {
		//		stagiaires = new ArrayList<>();
		if( root != null) {

			parcousInfixe2(root.getFilsGauche(), stagiaires);
			stagiaires.add(root);
			parcousInfixe2(root.getFilsDroit(), stagiaires);

		}
	}

	public NoeudStagiaire Get_Noeud(Stagiaire valRecherchee,NoeudStagiaire root) {

		if (root == null || valRecherchee == root.getVal()) {
			return root;
		}
		if (valRecherchee.compareTo(root.getVal())<0) {
			return Get_Noeud(valRecherchee, root.getFilsGauche());
		}
		return Get_Noeud(valRecherchee, root.getFilsDroit());
	}

	public NoeudStagiaire supprimer(Stagiaire valRecherchee, NoeudStagiaire a)
	{
		if (a == null)
			return a; //a: noeud courant;
		if (valRecherchee.compareTo(a.getVal()) == 0) {  

			return supprimerRacine(a);
		}
		if (valRecherchee.compareTo(a.getVal()) < 0) {
			a.setFilsGauche(supprimer(valRecherchee, a.getFilsGauche()));
		} else { 
			a.setFilsDroit(supprimer(valRecherchee, a.getFilsDroit()));
		}
		return a;
	}


	public NoeudStagiaire supprimerRacine(NoeudStagiaire a) 
	{
		if (a.getFilsGauche() == null)
			return a.getFilsDroit();
		if (a.getFilsDroit() == null)
			return a.getFilsGauche();
		NoeudStagiaire f = dernierDescendant(a.getFilsGauche());
		a.setVal(f.getVal());
		a.setFilsGauche(supprimer(f.getVal(), a.getFilsGauche()));
	        return f;
	}
	    

	public NoeudStagiaire dernierDescendant(NoeudStagiaire a) 
	{
		if (a.getFilsDroit() == null)
			return a;
		return dernierDescendant(a.getFilsDroit());
	}	
}
